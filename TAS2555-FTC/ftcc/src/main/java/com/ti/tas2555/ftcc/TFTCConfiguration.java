package com.ti.tas2555.ftcc;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.InvalidPropertiesFormatException;

/**
 * Created by a0220410 on 6/10/2016.
 */
public class TFTCConfiguration {
    private static final String TAG  = "TFTCConfiguration";
    private static final String CALIBRATION_TIME = "CALIBRATION_TIME";
    private static final String VERIFICATION_TIME = "VERIFICATION_TIME";
	private static final String CAL_DELTA_T = "CAL_DELTA_T";
    private static final String TEST_DELTA_T = "TEST_DELTA_T";
    private static final String CONFIGURATION = "CONFIGURATION";
    private static final String CONFIGURATION_CALIBRATION = "CONFIGURATION_CALIBRATION";
    private static final String SPK_T_MAX = "SPK_T_MAX";
    private static final String SPK_RE_TOL_PER = "SPK_RE_TOL_PER";
    private static final String SPK_RE_ALPHA = "SPK_RE_ALPHA";
    private static final String PPC3_RE0 = "PPC3_RE0";
    private static final String PPC3_RTV = "PPC3_RTV";
    private static final String PPC3_RTM = "PPC3_RTM";
    private static final String PPC3_RTVA = "PPC3_RTVA";
    private static final String PPC3_SYSGAIN = "PPC3_SYSGAIN";
    private static final String PPC3_DEV_NONLIN_PER = "PPC3_DEV_NONLIN_PER";
    private static final String PPC3_DELTA_T_LIMIT = "PPC3_DELTA_T_LIMIT";
    private static final String FS_RATE = "FS_RATE";
    private static final String RE_HI = "RE_HI";
    private static final String RE_LO = "RE_LO";
    private static final String F0_HI = "F0_HI";
    private static final String F0_LO = "F0_LO";
    private static final String Q_HI = "Q_HI";
    private static final String Q_LO = "Q_LO";
    private static final String T_HI = "T_HI";
    private static final String T_LO = "T_LO";
    private static final String NFS = "NFS";
    private static final String SCTH = "SCTH";
    public static final String Re = "Re";
    public static final String F0 = "f0";
    public static final String Q = "Q";
    public static final String Delta_tv = "delta_tv";

    public static final int RESULT_PASS = 0x00000000;
    public static final int RE_FAIL_HI = 0x00000001;
    public static final int RE_FAIL_LO = 0x00000010;
    public static final int F0_FAIL_HI = 0x00000100;
    public static final int F0_FAIL_LO = 0x00001000;
    public static final int Q_FAIL_HI  = 0x00010000;
    public static final int Q_FAIL_LO  = 0x00100000;
    public static final int T_FAIL_HI  = 0x01000000;
    public static final int T_FAIL_LO  = 0x10000000;

    public boolean bVerbose;
    public boolean bLoadCalibration;

    public int nCalibrationTime;
    public int nVerificationTime;

    public double nNFS;
    public double nSCTH;

    public boolean bFTCBypass;
    public double nTestDeltaT;
	public double nCalDeltaT;
	public boolean bCalDeltaTPresent;
    public int nConfiguration;
    public int nConfigurationCalibration;

    public double nSpkTMax;
    public double nSpkReTolPer;
    public double nSpkReAlpha;

    public double nPPC3_Re0;
    public double nPPC3_RTV;
    public double nPPC3_RTM;
    public double nPPC3_RTVA;
    public double nPPC3_SysGain;
    public double nPPC3_DevNonlinPer;
    public double nPPC3_DeltaTLimit;
    public int nFSRate;

    public double nReHi;
    public double nReLo;
    public double nF0Hi;
    public double nF0Lo;
    public double nQHi;
    public double nQLo;
    public double nTHi;
    public double nTLo;

    public double nRe;
    public double nF0;
    public double nQ;
    private double nTCal;
    public double ntest_delta_tv;
    public int nResult;
    public int nprm_pow;
    public int nprm_tlimit;

    private int MAX_BIN_SIZE = 2048;
    private byte[] gpBin = new byte[MAX_BIN_SIZE];
    private int gnBinIndex = 0;
    private int gnBinBlockIndex = 0;

	public void setTCal(double t_cal)
	{
		nTCal = t_cal;
		if(bCalDeltaTPresent == false){
			nCalDeltaT = nSpkTMax - nTCal;
		}
	}
	
	public double getTCal(){
		return nTCal;
	}
	
    public void LoadFTCC(String filename)throws java.io.IOException{
		bCalDeltaTPresent = false;
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        while (bufferedReader.ready()){
            String line = bufferedReader.readLine();
            if(line == null) break;
            if(line.isEmpty() || line.startsWith(";")) continue;

            int idx = line.indexOf(';');
            if(idx != -1) line = line.substring(0, idx -1);
            ftcc_parse(line);
        }

		if(bCalDeltaTPresent == false){
			nCalDeltaT = nSpkTMax - nTCal;
		}
        bufferedReader.close();
        fileReader.close();
    }

    private void ftcc_parse(String line) throws InvalidPropertiesFormatException{
        if(line.contains(CALIBRATION_TIME))
            nCalibrationTime = getInt(CALIBRATION_TIME, line);
        else if(line.contains(VERIFICATION_TIME))
            nVerificationTime = getInt(VERIFICATION_TIME, line);
        else if(line.contains(TEST_DELTA_T))
            nTestDeltaT = getFloat(TEST_DELTA_T, line);
		else if(line.contains(CAL_DELTA_T)){
			bCalDeltaTPresent = true;
            nCalDeltaT = getFloat(CAL_DELTA_T, line);
		}else if(line.contains(CONFIGURATION_CALIBRATION))
            nConfigurationCalibration = getInt(CONFIGURATION_CALIBRATION, line);
        else if(line.contains(CONFIGURATION))
            nConfiguration = getInt(CONFIGURATION, line);
        else if(line.contains(SPK_T_MAX))
            nSpkTMax = getFloat(SPK_T_MAX, line);
        else if(line.contains(SPK_RE_TOL_PER))
            nSpkReTolPer = getFloat(SPK_RE_TOL_PER, line);
        else if(line.contains(SPK_RE_ALPHA))
            nSpkReAlpha = getFloat(SPK_RE_ALPHA, line);
        else if(line.contains(PPC3_RE0))
            nPPC3_Re0 = getFloat(PPC3_RE0, line);
        else if(line.contains(PPC3_RTVA))
            nPPC3_RTVA = getFloat(PPC3_RTVA, line);
        else if(line.contains(PPC3_RTV))
            nPPC3_RTV = getFloat(PPC3_RTV, line);
        else if(line.contains(PPC3_RTM))
            nPPC3_RTM = getFloat(PPC3_RTM, line);
        else if(line.contains(PPC3_SYSGAIN))
            nPPC3_SysGain = getFloat(PPC3_SYSGAIN, line);
        else if(line.contains(PPC3_DEV_NONLIN_PER))
            nPPC3_DevNonlinPer = getFloat(PPC3_DEV_NONLIN_PER, line);
        else if(line.contains(PPC3_DELTA_T_LIMIT))
            nPPC3_DeltaTLimit = getFloat(PPC3_DELTA_T_LIMIT, line);
        else if(line.contains(FS_RATE))
            nFSRate = getInt(FS_RATE, line);
        else if(line.contains(RE_HI))
            nReHi = getFloat(RE_HI, line);
        else if(line.contains(RE_LO))
            nReLo = getFloat(RE_LO, line);
        else if(line.contains(F0_HI))
            nF0Hi = getFloat(F0_HI, line);
        else if(line.contains(F0_LO))
            nF0Lo = getFloat(F0_LO, line);
        else if(line.contains(Q_HI))
            nQHi = getFloat(Q_HI, line);
        else if(line.contains(Q_LO))
            nQLo = getFloat(Q_LO, line);
        else if(line.contains(T_HI))
            nTHi = getFloat(T_HI, line);
        else if(line.contains(T_LO))
            nTLo = getFloat(T_LO, line);
        else if(line.contains(NFS))
            nNFS = getFloat(NFS, line);
        else if(line.contains(SCTH))
            nSCTH = getFloat(SCTH, line);
    }

    private int getInt(String key, String line) throws InvalidPropertiesFormatException{
        int val = 0;
        int idx = line.lastIndexOf('=');
        if(idx < key.length())
            throw new InvalidPropertiesFormatException(line);

        String convert = line.substring(idx + 1);
        convert.replace('\r', ' ');
        convert.replace('\n', ' ');
        convert.replace('\t', ' ');
        convert = convert.trim();

        val = Integer.valueOf(convert);

        return val;
    }

    private float getFloat(String key, String line)throws InvalidPropertiesFormatException{
        float val = 0;
        int idx = line.lastIndexOf('=');
        if(idx < key.length())
            throw new InvalidPropertiesFormatException(line);

        String convert = line.substring(idx + 1);
        convert.replace('\r', ' ');
        convert.replace('\n', ' ');
        convert.replace('\t', ' ');
        convert = convert.trim();

        val = Float.valueOf(convert);
        return val;
    }

    public int check_spk_bounds(double re, double f0, double q, double test_delta_tv)
    {
        int result = RESULT_PASS;

        if(re>nReHi)
            result |= RE_FAIL_HI;
        if(re<nReLo)
            result |= RE_FAIL_LO;
        if(f0>nF0Hi)
            result |= F0_FAIL_HI;
        if(f0<nF0Lo)
            result |= F0_FAIL_LO;
        if(q>nQHi)
            result |= Q_FAIL_HI;
        if(q<nQLo)
            result |= Q_FAIL_LO;
        if(test_delta_tv>nTHi)
            result |= T_FAIL_HI;
        if(test_delta_tv<nTLo)
            result |= T_FAIL_LO;

        return result;
    }

    public void saveCalResult(String file) throws java.io.IOException{
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        bufferedWriter.write(String.format("Re = %1.2f\n\r", nRe));
        bufferedWriter.write(String.format("F0 = %3.0f\n\r", nF0));
        bufferedWriter.write(String.format("Q  = %1.3f\n\r", nQ));
        bufferedWriter.write(String.format("t0 = %2.2f\n\r\n\r", nTCal));
        bufferedWriter.write(String.format("rms_pow       = 0x%08X\n\r", nprm_pow));
        bufferedWriter.write(String.format("t_limit       = 0x%08X\n\r", nprm_tlimit));
        bufferedWriter.write(String.format("test_delta_tv = %2.2f C\n\r", ntest_delta_tv));
        bufferedWriter.write(String.format("result        = 0x%08X\n\r", nResult));

        bufferedWriter.flush();
        bufferedWriter.close();
        fileWriter.close();
    }

    public void saveCalFirmware(String file, byte[] fwData) throws java.io.IOException{
        FileOutputStream fileOutputStream = new FileOutputStream(file, false);

        PrepareCalFWHeader();

        for(int i=0; i < fwData.length; i++){
            gpBin[gnBinIndex++] = fwData[i];
        }

        PrepareClosure();

        fileOutputStream.write(gpBin, 0, gnBinIndex);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    private void PrepareCalFWHeader(){
        String datafile = "Calibration Data File";
        String description = "Calibration Data File for TAS2555";
        gpBin[3] = '2';
        gpBin[2] = '5';
        gpBin[1] = '5';
        gpBin[0] = '5';

        for(int i=0; i < datafile.length(); i++){
            gpBin[24+i] = (byte) datafile.charAt(i);
        }

        for(int i=0; i < description.length(); i++){
            gpBin[24 + 64 + i] = (byte)description.charAt(i);
        }


        gnBinIndex += 24 + 64 + description.length() + 1;

        gnBinIndex +=
                4 +	//device family index
                        4 +	//device index
                        2 +	//num PLL index
                        0 +	//array PLL index
                        2 +	//num programs index
                        0 +	//array programs index
                        2 + 	//num configurations index
                        0;	//array configurations index

        gpBin[gnBinIndex++] = 0x00;
        gpBin[gnBinIndex++] = 0x01; // one calibration data block

        String CalibrationData = "Calibration Data";
        for(int i=0; i < CalibrationData.length(); i++){
            gpBin[gnBinIndex+ i] = (byte)CalibrationData.charAt(i);
        }
        gnBinIndex += 64;

        String CalibrationData2555 = "Calibration Data for TAS2555";
        for(int i=0; i < CalibrationData2555.length(); i++){
            gpBin[gnBinIndex+ i] = (byte)CalibrationData2555.charAt(i);
        }
        gnBinIndex += CalibrationData2555.length() + 1;

        gpBin[gnBinIndex++] = 0x00; // compatible program = smart amp (index 0)
        gpBin[gnBinIndex++] = (byte)nConfiguration; // compatible configuration

        gpBin[gnBinIndex++] = 0x00;
        gpBin[gnBinIndex++] = 0x00;
        gpBin[gnBinIndex++] = 0x00;
        gpBin[gnBinIndex++] = 0x0A; // block type = 0x0A (calibration)
        gnBinBlockIndex = gnBinIndex;

        gnBinIndex += 4; // number of commands index
    }

    private void PrepareClosure(){
        int nCommands;
        byte pCommit[] = {
            0x00, 0x04, (byte) 0x85, 0x00,
            (byte) 0x8C, 0x19, 0x7C, 0x00,
            0x00, 0x00, 0x01, 0x00
        };

        // write the commit sequence
        for(int i=0; i < pCommit.length; i++){
            gpBin[gnBinIndex++] = pCommit[i];
        }

        nCommands = ((gnBinIndex - gnBinBlockIndex) / 4) - 1;

        // write number of commands for calibration block
        gpBin[gnBinBlockIndex++] = (byte) ((nCommands & 0xFF000000) >> 24);
        gpBin[gnBinBlockIndex++] = (byte)((nCommands & 0x00FF0000) >> 16);
        gpBin[gnBinBlockIndex++] = (byte)((nCommands & 0x0000FF00) >> 8);
        gpBin[gnBinBlockIndex++] = (byte)(nCommands & 0x000000FF);

        // write bin file size
        gpBin[4] = (byte)((gnBinIndex & 0xFF000000) >> 24);
        gpBin[5] = (byte)((gnBinIndex & 0x00FF0000) >> 16);
        gpBin[6] = (byte)((gnBinIndex & 0x0000FF00) >> 8);
        gpBin[7] = (byte)((gnBinIndex & 0x000000FF));
    }
}
