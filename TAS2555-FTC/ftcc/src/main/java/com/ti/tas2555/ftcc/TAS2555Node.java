package com.ti.tas2555.ftcc;

import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.security.PublicKey;

/**
 * Created by a0220410 on 6/10/2016.
 */
public class TAS2555Node {
    static{
        System.loadLibrary("TAS2555ftc");
    }

    private native void Open(String devnode);
    private native void Close();
    private native void setSCTH(double nSCTH);
    private native void setRe(double ppc3_re0, double re, double alpha);
    private native void setNFS(double nNFS);
    private native double getRe(double ppc3_re0);
    private native double getF0(int fs);
    private native double getQ(int fs);
    private native double getPiGain(double nRTV, double nRTM, double nRTVA);
    private native void setTempCal(int prm_pow, int prm_tlimit);
    private native int CalcPrmPow(double re, double delta_t_max, double nRTV, double nRTM, double nRTVA, double nSysGain);
    private native int calcPrmTlimit(double delta_t_max, double alpha, double nDevNonlinPer, double nRTV, double nRTM, double nRTVA);
    private native void setConfiguration(int configuration);
    private native void setCalibration(int calibration);
    private native byte[] GetFWContent();

    public TAS2555Node(){
    }

    public void set_SCTH(double nSCTH){
        setSCTH(nSCTH);
    }

    public void set_Re(double ppc3_re0, double re, double alpha){
        setRe(ppc3_re0, re, alpha);
    }

    public void set_NFS(double nNFS){
        setNFS(nNFS);
    }

    public double get_Re(double ppc3_re0){
        return getRe(ppc3_re0);
    }

    public double get_f0(int fs){
        return getF0(fs);
    }

    public double get_Q(int fs){
        return getQ(fs);
    }

    public double get_pi_gain(double nRTV, double nRTM, double nRTVA){
        return getPiGain(nRTV, nRTM, nRTVA);
    }

    public void set_TempCal(int prm_pow, int prm_tlimit){
        setTempCal(prm_pow, prm_tlimit);
    }

    public int Calc_Prm_Pow(double re, double delta_t_max, double nRTV, double nRTM, double nRTVA, double nSysGain){
        return CalcPrmPow(re, delta_t_max, nRTV, nRTM, nRTVA, nSysGain);
    }

    public int calc_Prm_Tlimit(double delta_t_max, double alpha, double nDevNonlinPer, double nRTV, double nRTM, double nRTVA){
        return calcPrmTlimit(delta_t_max, alpha, nDevNonlinPer, nRTV, nRTM, nRTVA);
    }

    public void set_Configuration(int configuration){
        setConfiguration(configuration);
    }

    public void set_Calibration(int calibration){
        setCalibration(calibration);
    }

    public void OpenNode(String devnode){
        Open(devnode);
    }

    public void CloseNode(){
        Close();
    }

    public byte[] getFwContent(){return  GetFWContent();}
}
