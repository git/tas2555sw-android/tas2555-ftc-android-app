package com.ti.tas2555.tas2555_ftc;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.ti.tas2555.ftcc.TFTCConfiguration;

/**
 * Created by a0220410 on 6/17/2016.
 */
public class FTCListViewAdapter extends BaseAdapter {
    private TFTCConfiguration mTFTConfig;
    private LayoutInflater mInflater;

    public FTCListViewAdapter(Context context,TFTCConfiguration configuration){
        mTFTConfig = configuration;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item, null);
        }
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_list_item_title);
        TextView tvHigh = (TextView) convertView.findViewById(R.id.tv_list_item_high);
        TextView tvLow = (TextView) convertView.findViewById(R.id.tv_list_item_low);
        TextView tvVal = (TextView) convertView.findViewById(R.id.tv_list_item_val);
        TextView tvPass = (TextView) convertView.findViewById(R.id.tv_list_item_result);
        if(position == 0) {
            tvTitle.setText(mTFTConfig.Re);
            tvHigh.setText("Hi:" + String.format("%1.2f", mTFTConfig.nReHi));
            tvLow.setText("Lo:" + String.format("%1.2f", mTFTConfig.nReLo));
            tvVal.setText("Act:" + String.format("%1.2f", mTFTConfig.nRe));
            if((mTFTConfig.nResult & mTFTConfig.RE_FAIL_HI) != 0){
                tvPass.setText("Hi");
                tvPass.setBackgroundColor(Color.RED);
            }else if((mTFTConfig.nResult & mTFTConfig.RE_FAIL_LO) != 0){
                tvPass.setText("Lo");
                tvPass.setBackgroundColor(Color.RED);
            }else{
                tvPass.setText("Pass");
                tvPass.setBackgroundColor(Color.GREEN);
            }
        }else if(position == 1){
            tvTitle.setText(mTFTConfig.F0);
            tvHigh.setText("Hi:" + String.format("%3.0f", mTFTConfig.nF0Hi));
            tvLow.setText("Lo:" + String.format("%3.0f", mTFTConfig.nF0Lo));
            tvVal.setText("Act:" + String.format("%3.0f", mTFTConfig.nF0));
            if((mTFTConfig.nResult & mTFTConfig.F0_FAIL_HI) != 0){
                tvPass.setText("Hi");
                tvPass.setBackgroundColor(Color.RED);
            }else if((mTFTConfig.nResult & mTFTConfig.F0_FAIL_LO) != 0){
                tvPass.setText("Lo");
                tvPass.setBackgroundColor(Color.RED);
            }else{
                tvPass.setText("Pass");
                tvPass.setBackgroundColor(Color.GREEN);
            }
        }else if(position == 2){
            tvTitle.setText(mTFTConfig.Q);
            tvHigh.setText("Hi:" + String.format("%1.3f", mTFTConfig.nQHi));
            tvLow.setText("Lo:" + String.format("%1.3f",mTFTConfig.nQLo));
            tvVal.setText("Act:" + String.format("%1.3f",mTFTConfig.nQ));
            if((mTFTConfig.nResult & mTFTConfig.Q_FAIL_HI) != 0){
                tvPass.setText("Hi");
                tvPass.setBackgroundColor(Color.RED);
            }else if((mTFTConfig.nResult & mTFTConfig.Q_FAIL_LO) != 0){
                tvPass.setText("Lo");
                tvPass.setBackgroundColor(Color.RED);
            }else{
                tvPass.setText("Pass");
                tvPass.setBackgroundColor(Color.GREEN);
            }
        }else if(position == 3){
            tvTitle.setText(mTFTConfig.Delta_tv);
            tvHigh.setText("Hi:" + String.format("%2.2f",mTFTConfig.nTHi));
            tvLow.setText("Lo:" + String.format("%2.2f",mTFTConfig.nTLo));
            tvVal.setText("Act:" + String.format("%2.2f",mTFTConfig.ntest_delta_tv));
            if((mTFTConfig.nResult & mTFTConfig.T_FAIL_HI) != 0){
                tvPass.setText("Hi");
                tvPass.setBackgroundColor(Color.RED);
            }else if((mTFTConfig.nResult & mTFTConfig.T_FAIL_LO) != 0){
                tvPass.setText("Lo");
                tvPass.setBackgroundColor(Color.RED);
            }else{
                tvPass.setText("Pass");
                tvPass.setBackgroundColor(Color.GREEN);
            }
        }

        return convertView;
    }
}
