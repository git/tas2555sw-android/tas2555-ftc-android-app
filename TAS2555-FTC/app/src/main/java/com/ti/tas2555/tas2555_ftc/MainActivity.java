package com.ti.tas2555.tas2555_ftc;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.ti.tas2555.ftcc.TAS2555Node;
import com.ti.tas2555.ftcc.TFTCConfiguration;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    private String TAG = "TAS2555-FTC";
    private TAS2555Node mDevNode;
    private TFTCConfiguration mFTCConfiguration;
    private Button mStartButton;
    private Context mContext;
    private AudioManager mAudioManager;
    private int mAudioStreamMaxVolume;
    private int mAudioStreamCurVolume;
    private MediaPlayer mMediaPlayer;
    private Timer mTimer;
    private Cal_State mCalState = Cal_State.IDLE;
    private ListView mFTCItem;
    private FTCListViewAdapter mAdapter;
    private EditText mInfo;

    private  enum Cal_State{
        IDLE,
        CALIBRATE,
        VARIFY
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        mContext = getApplicationContext();
        mFTCItem = (ListView)findViewById(R.id.lvFTCItem);
        mTimer = new Timer("PlaybackTimer");
        mDevNode = new TAS2555Node();
        mFTCConfiguration = new TFTCConfiguration();
        mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        mAudioStreamMaxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mAudioStreamCurVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        mAdapter = new FTCListViewAdapter(getApplicationContext(), mFTCConfiguration);
        mFTCItem.setAdapter(mAdapter);
        mStartButton = (Button) findViewById(R.id.bnStart);
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FTCStart();
            }
        });
        mInfo = (EditText)findViewById(R.id.dtInfo);

        mInfo.setText("please put TAS2555_cal_m28dB.wav, TAS2555_verify_m06.wav, speaker.ftcfg to /data before run FTC\n");
        mInfo.setSelection(mInfo.getText().length());
    }

    private boolean checkfileExist(){
        File file1=new File("/data/TAS2555_cal_m28dB.wav");
        if(file1.exists())
            mInfo.append(file1.getAbsolutePath() + " found\n");
        else
            mInfo.append(file1.getAbsolutePath() + " not found\n");

        mInfo.setSelection(mInfo.getText().length());

        File file2=new File("/data/TAS2555_verify_m06.wav");
        if(file2.exists())
            mInfo.append(file2.getAbsolutePath() + " found\n");
        else
            mInfo.append(file2.getAbsolutePath() + " not found\n");

        mInfo.setSelection(mInfo.getText().length());

        File file3=new File("/data/speaker.ftcfg");
        if(file3.exists())
            mInfo.append(file3.getAbsolutePath() + " found\n");
        else
            mInfo.append(file3.getAbsolutePath() + " not found\n");

        mInfo.setSelection(mInfo.getText().length());
        if(file1.exists() && file2.exists() && file3.exists())
            return true;
        else
            return false;
    }

    @Override
    protected void onStop() {
        if(mMediaPlayer != null) mMediaPlayer.release();
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioStreamCurVolume, 0);
        mTimer.cancel();
        mDevNode.CloseNode();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void FTCStart(){
        if(!checkfileExist())
            return;

        try {
            mFTCConfiguration.LoadFTCC("/data/speaker.ftcfg");
            mInfo.append("/data/speaker.ftcfg loaded\n");
            mInfo.setSelection(mInfo.getText().length());
            mInfo.append("calibration configuration :" + mFTCConfiguration.nConfigurationCalibration + "\n");
            mInfo.setSelection(mInfo.getText().length());
            mInfo.append("calibration time :" + mFTCConfiguration.nCalibrationTime + "\n");
            mInfo.setSelection(mInfo.getText().length());
            mInfo.append("verify time :" + mFTCConfiguration.nVerificationTime+ "\n");
            mInfo.setSelection(mInfo.getText().length());
            mAdapter.notifyDataSetChanged();
            mDevNode.OpenNode("/dev/tiload_node");
            mInfo.append("/dev/tiload_node opened \n");
            mInfo.setSelection(mInfo.getText().length());
            mFTCConfiguration.setTCal(25.0);
            mInfo.append("ambient temperature :" + mFTCConfiguration.getTCal() + "\n");
            mInfo.setSelection(mInfo.getText().length());
            mFTCConfiguration.bLoadCalibration = false;
            mInfo.append("reload calibration :" + mFTCConfiguration.bLoadCalibration + "\n");
            mInfo.setSelection(mInfo.getText().length());
            // STEP 1: Load TAS2555 calibration configuration
            mInfo.append("STEP1: change configuration to " + mFTCConfiguration.nConfigurationCalibration + "\n");
            mInfo.setSelection(mInfo.getText().length());
            mDevNode.set_Configuration(mFTCConfiguration.nConfigurationCalibration);
            File file=new File("/data/TAS2555_cal_m28dB.wav");
            if(file.exists()){
                mMediaPlayer = new MediaPlayer();
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioStreamMaxVolume, 0);
                mMediaPlayer.setDataSource(file.getAbsolutePath());
                mCalState = Cal_State.CALIBRATE;
                mMediaPlayer.setOnPreparedListener(this);
                mInfo.append("preparing " + file.getAbsolutePath() + "\n");
                mInfo.setSelection(mInfo.getText().length());
                mMediaPlayer.prepareAsync();
            }
        }catch (Exception ex){
            Log.e(TAG, ex.toString());
        }
    }

    public void onPrepared(MediaPlayer mp) {
        if(mCalState == Cal_State.CALIBRATE) {
            // STEP 2: Play calibration signal
            mInfo.append("STEP2: start play /data/TAS2555_cal_m28dB.wav \n");
            mInfo.setSelection(mInfo.getText().length());
            mMediaPlayer.setScreenOnWhilePlaying(true);
            mMediaPlayer.start();
            try {
                Thread.sleep(500);
            }catch (Exception ex){
                Log.e(TAG, ex.toString());
            }

            // STEP 3: Re-program Re for worst case
            mInfo.append("STEP3: Re-program Re for worst case \n");
            mInfo.setSelection(mInfo.getText().length());
            double re = mFTCConfiguration.nPPC3_Re0;
            re = mFTCConfiguration.nPPC3_Re0 * (1 - 2 * mFTCConfiguration.nSpkReTolPer / 100.0);			
			int prm_pow = mDevNode.Calc_Prm_Pow(re, 
					mFTCConfiguration.nCalDeltaT, 
					mFTCConfiguration.nPPC3_RTV, 
					mFTCConfiguration.nPPC3_RTM, 
					mFTCConfiguration.nPPC3_RTVA, 
					mFTCConfiguration.nPPC3_SysGain);
			int prm_tlimit = mDevNode.calc_Prm_Tlimit(mFTCConfiguration.nCalDeltaT, 
					mFTCConfiguration.nSpkReAlpha, 
					mFTCConfiguration.nPPC3_DevNonlinPer, 
					mFTCConfiguration.nPPC3_RTV, 
					mFTCConfiguration.nPPC3_RTM, 
					mFTCConfiguration.nPPC3_RTVA);
            mDevNode.set_Re(mFTCConfiguration.nPPC3_Re0, re, mFTCConfiguration.nSpkReAlpha);
			mDevNode.set_TempCal(prm_pow, prm_tlimit);
            mDevNode.set_NFS(mFTCConfiguration.nNFS);
            mDevNode.set_SCTH(mFTCConfiguration.nSCTH);

            // STEP 4: Wait for algorithm to converge
            mInfo.append("STEP4: Wait " + mFTCConfiguration.nCalibrationTime +"ms for algorithm to converge \n");
            mInfo.setSelection(mInfo.getText().length());
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    CalTimeReached();
                }
            }, mFTCConfiguration.nCalibrationTime);
        }else if(mCalState == Cal_State.VARIFY){
            mInfo.append("start play /data/TAS2555_verify_m06.wav \n");
            mInfo.setSelection(mInfo.getText().length());
            mMediaPlayer.start();
            try {
                Thread.sleep(500);
            }catch (Exception ex){
                Log.e(TAG, ex.toString());
            }

            mInfo.append("Wait " + mFTCConfiguration.nVerificationTime +" ms for verify \n");
            mInfo.setSelection(mInfo.getText().length());
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    VerifyTimeReached();
                }
            }, mFTCConfiguration.nVerificationTime);
        }
    }

    private void CalTimeReached(){
        // STEP 5: Get actual Re, f0 and Q from TAS2555
        mFTCConfiguration.nRe = mDevNode.get_Re(mFTCConfiguration.nPPC3_Re0);
        mFTCConfiguration.nF0 = mDevNode.get_f0(mFTCConfiguration.nFSRate);
        mFTCConfiguration.nQ = mDevNode.get_Q(mFTCConfiguration.nFSRate);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
                mInfo.append("STEP5: Get actual Re, f0 and Q from TAS2555 \n");
                mInfo.setSelection(mInfo.getText().length());
            }
        });

        // STEP 6: Verify Speaker at gpFTCC->nTestDeltaT
        int prm_pow = mDevNode.Calc_Prm_Pow(mFTCConfiguration.nRe,
                mFTCConfiguration.nTestDeltaT,
                mFTCConfiguration.nPPC3_RTV,
                mFTCConfiguration.nPPC3_RTM,
                mFTCConfiguration.nPPC3_RTVA,
                mFTCConfiguration.nPPC3_SysGain);
        int prm_tlimit = mDevNode.calc_Prm_Tlimit(
                mFTCConfiguration.nTestDeltaT,
                mFTCConfiguration.nSpkReAlpha,
                mFTCConfiguration.nPPC3_DevNonlinPer,
                mFTCConfiguration.nPPC3_RTV,
                mFTCConfiguration.nPPC3_RTM,
                mFTCConfiguration.nPPC3_RTVA);

        mDevNode.set_Re(mFTCConfiguration.nPPC3_Re0,
                mFTCConfiguration.nRe,
                mFTCConfiguration.nSpkReAlpha);
        mDevNode.set_TempCal(prm_pow, prm_tlimit);

        mMediaPlayer.stop();
        mMediaPlayer.release();
        mMediaPlayer = null;

        StartVerify();
    }

    private void StartVerify(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mInfo.append("STEP6: Verify Speaker at gpFTCC->nTestDeltaT \n");
                mInfo.setSelection(mInfo.getText().length());
            }
        });

        try {
            File file=new File("/data/TAS2555_verify_m06.wav");
            if(file.exists()){
                mMediaPlayer = new MediaPlayer();
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.setDataSource(file.getAbsolutePath());
                mCalState = Cal_State.VARIFY;
                mMediaPlayer.setOnPreparedListener(this);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mInfo.append("preparing TAS2555_verify_m06.wav \n");
                        mInfo.setSelection(mInfo.getText().length());
                    }
                });
                mMediaPlayer.prepareAsync();
            }
        }catch (Exception ex){
            Log.e(TAG, ex.toString());
        }
    }

    private void VerifyTimeReached(){
        double re = mDevNode.get_Re(mFTCConfiguration.nPPC3_Re0);
        mFTCConfiguration.ntest_delta_tv = (re/mFTCConfiguration.nRe - 1) / mFTCConfiguration.nSpkReAlpha;

        mFTCConfiguration.nResult = mFTCConfiguration.check_spk_bounds(
                mFTCConfiguration.nRe,
                mFTCConfiguration.nF0,
                mFTCConfiguration.nQ,
                mFTCConfiguration.ntest_delta_tv);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
                mInfo.append("verify time reached \n");
                mInfo.setSelection(mInfo.getText().length());
                mInfo.append("STEP7: Set temperature limit to target TMAX \n");
                mInfo.setSelection(mInfo.getText().length());
            }
        });

        // STEP 7: Set temperature limit to target TMAX
        mFTCConfiguration.nprm_pow = mDevNode.Calc_Prm_Pow(
                mFTCConfiguration.nRe,
                mFTCConfiguration.nSpkTMax - mFTCConfiguration.getTCal(),
                mFTCConfiguration.nPPC3_RTV,
                mFTCConfiguration.nPPC3_RTM,
                mFTCConfiguration.nPPC3_RTVA,
                mFTCConfiguration.nPPC3_SysGain);

        mFTCConfiguration.nprm_tlimit = mDevNode.calc_Prm_Tlimit(
                mFTCConfiguration.nSpkTMax - mFTCConfiguration.getTCal(),
                mFTCConfiguration.nSpkReAlpha,
                mFTCConfiguration.nPPC3_DevNonlinPer,
                mFTCConfiguration.nPPC3_RTV,
                mFTCConfiguration.nPPC3_RTM,
                mFTCConfiguration.nPPC3_RTVA);

        mDevNode.set_Re(
                mFTCConfiguration.nPPC3_Re0,
                mFTCConfiguration.nRe,
                mFTCConfiguration.nSpkReAlpha);

        mDevNode.set_TempCal(
                mFTCConfiguration.nprm_pow,
                mFTCConfiguration.nprm_tlimit);

        mMediaPlayer.stop();
        mMediaPlayer.release();
        mMediaPlayer = null;
        PostCalibration();
    }

    private void PostCalibration(){
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mInfo.append("STEP8: Save Re, f0, Q and Cal Temp into " + getApplicationContext().getFilesDir().getAbsolutePath() +  "/tas2555_cal.txt \n");
                    mInfo.setSelection(mInfo.getText().length());
                }
            });
            // STEP 8: Save Re, f0, Q and Cal Temp into a file
            File file = mContext.getFilesDir();
            mFTCConfiguration.saveCalResult(file.getAbsolutePath() +  "/tas2555_cal.txt");

            // STEP 9: Save .bin file for TAS2555 driver
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mInfo.append("mFTCConfiguration.nResult = " + String.format("0x%x", mFTCConfiguration.nResult) + "\n");
                    mInfo.setSelection(mInfo.getText().length());
                }
            });

            if (mFTCConfiguration.nResult == mFTCConfiguration.RESULT_PASS)
            {
                byte[] fwContent = mDevNode.getFwContent();
                mFTCConfiguration.saveCalFirmware(file.getAbsolutePath() + "/tas2555_cal.bin", fwContent);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mInfo.append(mContext.getFilesDir().getAbsolutePath() + "/tas2555_cal.bin saved \n");
                        mInfo.setSelection(mInfo.getText().length());
                        if(mFTCConfiguration.bLoadCalibration) {
                            mInfo.append("Load Calibration \n");
                            mInfo.setSelection(mInfo.getText().length());
                        }
                    }
                });

                if(mFTCConfiguration.bLoadCalibration) {
                    mDevNode.set_Calibration(0xff);
                }
            }

            mDevNode.CloseNode();
        }catch (Exception ex){
            Log.e(TAG, ex.toString());
        }
    }
}
